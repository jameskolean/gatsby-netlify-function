import fetch from "node-fetch"

exports.handler = async function(event, context) {
  const headers = {
    Accept: "application/jsonhtml",
  }

  try {
    const response = await fetch("https://randomuser.me/api/?results=3", {
      headers,
    })
    if (!response.ok) {
      return { statusCode: response.status, body: response.statusText }
    }
    const data = await response.json()
    return {
      statusCode: 200,
      body: JSON.stringify(data),
    }
  } catch (err) {
    console.log(err) // output to netlify function log
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: err.message }), // Could be a custom message or object i.e. JSON.stringify(err)
    }
  }
}
