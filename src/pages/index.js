import React, { useState } from "react"

const Users = ({ users }) => {
  const hasUsers = users && users.length > 0
  if (hasUsers) {
    const userRows = users.map(user => (
      <tr>
        <td>
          <img src={user.picture.thumbnail} alt="thumbnail" />
        </td>
        <td>{user.name.first}</td>
        <td>{user.name.last}</td>
      </tr>
    ))
    return (
      <table>
        <tr>
          <th></th>
          <th>Firstname</th>
          <th>Lastname</th>
        </tr>
        {userRows}
      </table>
    )
  }
  return <div>Lookup users</div>
}
const IndexPage = () => {
  const [functionMessage, setFunctionMessage] = useState("")
  const callHelloFunction = () => {
    setFunctionMessage("loading ...")
    fetch("/.netlify/functions/hello")
      .then(response => response.json())
      .then(data => setFunctionMessage(data.msg))
  }
  const [users, setUsers] = useState([])
  const callUsersFunction = () => {
    setUsers([])
    fetch("/.netlify/functions/users")
      .then(response => response.json())
      .then(data => setUsers(data.results))
  }
  return (
    <div>
      <h1>Netlify Function.</h1>
      <button type="button" onClick={callHelloFunction}>
        Run Hello Function
      </button>
      <p>Result: {functionMessage}</p>
      <button type="button" onClick={callUsersFunction}>
        Run Users Function
      </button>
      <Users users={users} />
    </div>
  )
}

export default IndexPage
